# 20200418-BalrajVerma-NYCSchools

Code Challange 

Version of Language : Swift 4.2
Xcode version : 11.4.1
Design Pattern : MVVM , Using POP, Bidirectional data flow thorugh Protocols 
orientaton : Portrait and Landescape 
Min deployment target: 13.0 
Designed for : iPhone 
Family tested : As suggested iPhone 10 but working well on all devices i tested.
Unit testing : Done, With good code coverage.
New thing which i tried in this assignment : NSCache
