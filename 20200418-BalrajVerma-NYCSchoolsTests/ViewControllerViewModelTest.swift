//
//  ViewControllerViewModelTest.swift
//  20200418-BalrajVerma-NYCSchoolsTests
//
//  Created by DataCore Inc  on 4/19/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import XCTest
@testable import _0200418_BalrajVerma_NYCSchools

final class ViewControllerViewModelTest: XCTestCase {
    var viewModel: ViewControllerViewModel!
    override func setUpWithError() throws {
        viewModel =  ViewControllerViewModel()
    }
    
    func testperformNYCFetch () {
        let expect = XCTestExpectation(description: "Download School List Expectation")
        guard let url = URL(string: APIUrls.schoolList) else {
             print("Invalid URL")
             return
         }
         URLSession.shared.dataTask(with: url) {[weak self] data, response, error in
             guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                 XCTFail("API is not working")
                 expect.fulfill()
                 return
             }
             if let json = data {
                 XCTAssert(true, "API is working")
                 expect.fulfill()
             }

         }.resume()
        wait(for: [expect], timeout: 10)
    }
    
    func testperformDetailsFetch () {
        let expect = XCTestExpectation(description: "Download School List Expectation")
        guard let url = URL(string: APIUrls.schoolDetail) else {
            print("Invalid URL")
            return
        }
        let request = URLRequest(url: url)
        print(request)
        URLSession.shared.dataTask(with: request) {[weak self] data, response, error in
            guard (response as? HTTPURLResponse)?.statusCode == 200 else {
                XCTFail("API is not working")
                expect.fulfill()
                return
            }
            if let json = data {
                XCTAssert(true, "API is working")
                expect.fulfill()
            }
        }.resume()
    }
    
    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
