//
//  ViewControllerTests.swift
//  20200418-BalrajVerma-NYCSchoolsTests
//
//  Created by DataCore Inc  on 4/18/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

//MARK: Step to run the Test case in the file.
/*
Select the Main.storyboard. -> Select View Controller scene -> Select the Identity Inspector - > Select Module and change it to TEST module. You are good to go.
*/

import XCTest
@testable import _0200418_BalrajVerma_NYCSchools

final class ViewControllerTests: XCTestCase {
    var viewController: ViewController!
    var navigationController: UINavigationController!
    var tableView:UITableView!
    
    override func setUpWithError() throws {
       // viewController = ViewController()
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let vc: ViewController = storyboard.instantiateViewController(withIdentifier: "ViewController") as! ViewController
        viewController = vc
        navigationController = UINavigationController(rootViewController: viewController)
        UIApplication.shared.keyWindow?.rootViewController = viewController
    }
    
    func testViewDidLoad() {
           XCTAssertNotNil(self.viewController?.view, "View not initiated properly")
           XCTAssertNotNil(self.viewController?.isViewLoaded)
       }
    func testOutLets() {
           XCTAssertNotNil(viewController.schoolListTableView)
       }
    
    func testDataSource() {
          XCTAssertNotNil(viewController?.schoolListTableView.dataSource)
      }
      
      func testDelegate() {
          XCTAssertNotNil(viewController?.schoolListTableView.delegate)
      }
    
    func testCellType(){
          if let tableView = viewController?.schoolListTableView {
              let firstCell = viewController?.tableView(tableView, cellForRowAt: IndexPath(row: 0, section: 0))
              XCTAssert(firstCell is SchoolListTableViewCell)
          }
      }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        self.measure {
            self.viewController?.viewDidLoad()
            self.viewController?.viewWillAppear(false)
        }
    }

}
