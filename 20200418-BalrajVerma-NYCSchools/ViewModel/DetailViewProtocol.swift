//
//  DetailViewProtocol.swift
//  20200418-BalrajVerma-NYCSchools
//
//  Created by DataCore Inc  on 4/18/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import Foundation

protocol DetailViewControllerConfigurables: AnyObject {
    var schoolSetData: NYCSchoolDetails? {get set}
}

///Thought not create a file for just two line of class used this one for below class.
final class DetailViewControllerViewModel: DetailViewControllerConfigurables {
    var schoolSetData: NYCSchoolDetails?
}
