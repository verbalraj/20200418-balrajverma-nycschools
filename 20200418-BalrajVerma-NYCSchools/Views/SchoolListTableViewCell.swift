//
//  SchoolListTableViewCell.swift
//  20200418-BalrajVerma-NYCSchools
//
//  Created by DataCore Inc  on 4/18/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//

import UIKit

final class SchoolListTableViewCell: UITableViewCell {
    @IBOutlet weak var schoolNameLabel: UILabel!
    @IBOutlet weak var cityLabel: UILabel!
    @IBOutlet weak var zipLabel: UILabel!
    @IBOutlet weak var stateCodeLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    var cellViewModel: SchoolListTableViewCellConfigurables! {
        didSet {
            if let name = cellViewModel.schoolName, let city = cellViewModel.city, let state = cellViewModel.state_code, let zip = cellViewModel.zip {
                self.schoolNameLabel.text = name
                self.cityLabel.text = city
                self.stateCodeLabel.text = state
                self.zipLabel.text = zip
            }
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    ///Return the class name in String format
    class func reUseID() -> String {
        return String(describing: self)
    }
}
