//
//  APIBuilder.swift
//  20200418-BalrajVerma-NYCSchools
//
//  Created by DataCore Inc  on 4/18/20.
//  Copyright © 2020 Com.test.Sample. All rights reserved.
//


///I think instead of returning a computed textual string i used the static one, As i learned if we use computed property in large amount init() command put memory burden on the app, While if you have limited textual information you can take benefit of static keyword here. Instead of init command.
enum APIUrls {
    // Endpoints
    static let schoolList = "https://data.cityofnewyork.us/resource/s3k6-pzi2.json" // Return school lists
    static let schoolDetail = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json" // Return school details
}
///Set Nav bar title used in app as per controller's name - I think new developer can frequently learn that pattern within team. 
enum InAppNavigationBarTitle {
    //If possible for ease please use class name for the title of nav bar
    static let viewControllerNavBarTitle = "NYC - List"
    static let detailViewControllerNavBarTitle = "SAT Score Details"
}
///Tried to give name of storyboard as they are.
enum InAppStoryBoardName {
    static let detailView = "DetailsView"
    static let detailViewIdentifier = "DetailViewController"
}
///Only used for various error/informtio message within App.
enum InAppErrorMessage {
    static let dataNotFoundError = "No SAT scores exist for this school, Please check json for more details."
    static let activityIndicatorLoadingTitle = "Loading..."
}

enum InAppAlertButtonTitle {
    static let alertTitle = "Error!"
    static let cancelTitle = "Cancel"
}

//API decodable data - Only picking up important values DBN kind of unique id ofschool which will be used to navigate to another screen.
struct NYCSchool: Decodable {
    let dbn: String?
    let school_name: String?
    let city: String?
    let zip: String?
    let state_code: String?
}

struct NYCSchoolDetails: Decodable {
    let dbn: String?
    let school_name: String?
    let num_of_sat_test_takers: String?
    let sat_critical_reading_avg_score: String?
    let sat_math_avg_score: String?
    let sat_writing_avg_score: String?
}
